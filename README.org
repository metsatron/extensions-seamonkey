* SeaMonkey Extensions
[[./screenshot.png]]

** About
Various SeaMonkey add-ons (not my own; mostly abandoned) that I have modified to work with 2.49+

- For security, parts of bug 1035091 were ported in bug 1411708. This might cause problems with add-ons like Stylish which use moz-document.  
- Set the pref "layout.css.moz-document.content.enabled" to "true" to overcome this but be aware that this disables the added security fix.

** Install
   :PROPERTIES:
   :CUSTOM_ID: install
   :END:
#+BEGIN_SRC sh
# Backup SeaMonkey Extensions
mv ~/.mozilla/seamonkey/**.default/extensions ~/.mozilla/seamonkey/**.default/extensions.bk
# Clone SeaMonkey Extensions Repo
git clone git@gitlab.com:metsatron/extensions-seamonkey.git ~/.mozilla/seamonkey/**.default/extensions
#+END_SRC

** Custom Buttons Code 
  * Name: Archive
  * Image: file://~/.icons/BeOS-r5/16x16/actions/dialog-ok.png
  * Code:
 #+BEGIN_EXAMPLE
 /*Code*/
 // toggle bookmarks sidebar
 var utils = document.commandDispatcher.focusedWindow.
   QueryInterface(Components.interfaces.nsIInterfaceRequestor).
   getInterface(Components.interfaces.nsIDOMWindowUtils);
 utils.sendKeyEvent("keypress", 0, KeyEvent.DOM_VK_A,
   Event.SHIFT_MASK);
 #+END_EXAMPLE

** Add-on List
  * Blender [converted]: blender@meh.paranoid.pk
  * CanvusBlocker [converted]: CanvasBlocker@kkapsner.de
  * ChatZilla: {59c81df5-4b7a-477b-912d-4e0fdf64e5f2}
  * Classic Password Editor: classicpasswordeditor@daniel.dawson
  * Complete YouTube Saver: {AF445D67-154C-4c69-A17B-7F392BCC36A3}
  * Cookies Experminator: CookiesExterminator@Off.JustOff
  * Custom Buttons: custombuttons-signed@infocatcher
  * Decentraleyes: jid1-BoFifL9Vbdl2zQ@jetpack
  * DownThemAll!: {DDC359D1-844A-42a7-9AA1-88A850A938A8}
  * EarlyBlue: EarlyBlue@kairo.at
  * Enigmail: {847b3a00-7ab1-11d4-8f02-006008948af5}
  * FireFTP: {a7c6cf7f-112c-4500-a7ea-39801a327e5f}
  * Ghostery: firefox@ghostery.com
  * GNOMErunner: {ed492c7d-322b-4da5-bdf1-aeec9fea8bda}
  * HTTPS Everywhere: https-everywhere@eff.org
  * I don't care about cookies [converted]: jid1-KKzOGWgsW3Ao4Q@jetpack
  * Lightbird: {df846912-2659-4705-acb0-8bc0c0520e1a}
  * Lightning: {3550f703-e582-4d05-9a08-453d09bdfdc6}
  * Modern2: {a9ecc489-5a88-4c3b-bc1f-a8066681bf1f}
  * NoScript: {73a6fe31-595d-460b-a920-fcc0f8843232}
  * Orbit 3+1 Revived: orbit@miksworld.de
  * Orbit Retro 2.12: {d647af09-86cb-11dc-8c76-001bb95520ac}
  * PassIFox [converted]: passifox@hanhuy.com
  * Pentadactyl [converted]: pentadactyl@dactyl.googlecode.com
  * Privacy Badger [converted]: jid1-MnnxcxisBPnSXQ@jetpack 
  * Privacy Tab: privateTab@infocatcher
  * S3.Google Translator: s3google@translator
  * [[https://gitlab.com/metsatron/graymodern-revived-mozilla.org][SeaMonkey Gray Modern Revived: graymodern-revived@mozilla.org]]
  * SeaMonkey Vertical Tab Bar [converted]: sm-vtabbar@rn10950
  * Speed Dial: {64161300-e22b-11db-8314-0800200c9a66}
  * Stylish: {46551EC9-40F0-4e47-8E18-8E5CF550CFB8}
  * Throbber Changer [converted]: {3b5adbb4-606f-4c3e-9a5c-7453ba47bfe3}
  * Toolbar Buttons [converted]: {03B08592-E5B4-45ff-A0BE-C1D975458688}
  * Torrent Tornado: s3torrent@tornado
  * uBlock: {2b10c1c8-a11f-4bad-fe9c-1c11e82cac42}
  * WebRTC Permissions UI Toggle: webrtc-permissions-ui-toggle@lakora.us
  * [[https://gitlab.com/metsatron/xpfeclassic-mozilla.org][XPFC-Classic: xpfeclassic@mozilla.org]]
